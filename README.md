# Web Crawler For RightMove

Console Command:

Result in tabular format
symfony console house-prices NW3 PRICE_DESC 0.0 30 ANY ANY --format=table

Result in json format
symfony console house-prices NW3 PRICE_DESC 0.0 30 ANY ANY --format=json


API:

http://localhost:8000/api/house-prices/NW3?sortBy=PRICE_DESC&radius=0.0&soldIn=30&propertyType=ANY&tenure=ANY


Unit Test:

php bin/phpunit
