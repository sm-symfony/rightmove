<?php


namespace App\Service;

use http\Exception\InvalidArgumentException;
use Symfony\Component\BrowserKit\HttpBrowser;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class HousePriceService
{
    const HOUSE_PRICE_URL = 'https://www.rightmove.co.uk/house-prices.html';
    const HOUSE_PRICE_AJAX_URL = 'https://www.rightmove.co.uk/house-prices/result';
    /**
     * @var HttpClientInterface
     */
    private $httpClient;

    public function __construct(HttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    /**
     * @param $searchTerm
     * @param $sortBy
     * @param $filters
     * @return array
     */
    public function search($searchTerm, $sortBy, $filters): array
    {
        $browser = new HttpBrowser(HttpClient::create());
        $crawler = $browser->request('GET', self::HOUSE_PRICE_URL);
        $form = $crawler->selectButton('housePrices')->form();
        $form['searchLocation'] = $searchTerm;
        $crawler = $browser->submit($form);

        $response = $crawler->filter('script')->eq(1)->text();
        $json = json_decode(strstr($response, '{'));

        if (!$json) {
            throw new InvalidArgumentException('Invalid Town Name, Postcode or Station');
        }

        $locationType = $json->searchLocation->locationType;
        $locationId = $json->searchLocation->locationId;

        $ajaxUrl = self::HOUSE_PRICE_AJAX_URL;

        // add sorting
        $ajaxUrl = $this->sortBy($ajaxUrl, $sortBy);

        // add filters
        $ajaxUrl = $this->addFilter($ajaxUrl, $filters);

        if (isset($locationType)) {
            $ajaxUrl .= "&locationType={$locationType}";
        }

        if (isset($locationId)) {
            $ajaxUrl .= "&locationId={$locationId}";
        }

        try {
            $response = $this->httpClient->request('GET', $ajaxUrl);
            $content = $response->getContent();
            $json = json_decode($content);
        } catch (TransportExceptionInterface | ClientExceptionInterface | RedirectionExceptionInterface | ServerExceptionInterface $e) {
            die($e->getMessage());
        }

        return $this->formatOutput($json);
    }

    /**
     * @param $ajaxUrl
     * @param $sortBy
     * @return string
     */
    private function sortBy($ajaxUrl, $sortBy): string
    {
        // default sorting by price desc to get expensive houses on top
        if (!isset($sortBy)) {
            $sortBy = 'PRICE_DESC';
        }

        if (isset($sortBy)) {
            $ajaxUrl .= "?sortBy={$sortBy}";
        }

        return $ajaxUrl;
    }

    /**
     * @param $ajaxUrl
     * @param $filters
     * @return string
     */
    private function addFilter($ajaxUrl, $filters): string
    {
        if (isset($filters['radius'])) {
            $ajaxUrl .= "&radius={$filters['radius']}";
        }

        if (isset($filters['soldIn'])) {
            $ajaxUrl .= "&radius={$filters['soldIn']}";
        }

        if (isset($filters['propertyType'])) {
            $ajaxUrl .= "&radius={$filters['propertyType']}";
        }

        if (isset($filters['tenure'])) {
            $ajaxUrl .= "&radius={$filters['tenure']}";
        }

        return $ajaxUrl;
    }

    /**
     * @param $json
     * @return array
     */
    private function formatOutput($json): array
    {
        $properties = array_slice($json->results->properties, 0, 5);
        $filteredProperties = [];

        foreach ($properties as $property) {
            $filteredProperties[] = [
                $property->address,
                $property->propertyType,
                $property->transactions[0]->displayPrice,
            ];
        }

        return [
            'number_of_sold_properties' => $json->results->resultCount,
            'properties' => $filteredProperties
        ];
    }
}