<?php

namespace App\Command;

use App\Service\HousePriceService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class HousePricesCommand extends Command
{
    protected static $defaultName = 'house-prices';
    protected static $defaultDescription = 'Search House Prices using \'town name\', \'postcode\' or \'station\'';
    /**
     * @var HousePriceService
     */
    private $housePriceService;

    public function __construct(HousePriceService $housePriceService)
    {
        parent::__construct();
        $this->housePriceService = $housePriceService;
    }

    protected function configure()
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument('searchTerm', InputArgument::REQUIRED, 'Town Name, Postcode or Station')
            ->addArgument('sortBy', InputArgument::OPTIONAL, 'ADDRESS, DEED_DATE, PRICE_ASC, PRICE_DESC')
            ->addArgument(
                'filter',
                InputArgument::IS_ARRAY | InputArgument::OPTIONAL,
                'Radius in mile [0.0, 0.25, 0.5, 1.0, 3.0, 5.0, 10.0, 15.0]
                Sold in last X years [1, 2, 5, 7, 30]
                Property Type [ANY, DETACHED, FLAT, SEMI_DETACHED, TERRACED, OTHER] 
                Tenure [ANY, FREEHOLD, LEASEHOLD]'
            )
            ->addOption('format', null, InputOption::VALUE_OPTIONAL, 'Format can be json or table');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $searchTerm = $input->getArgument('searchTerm');
        $sortBy = $input->getArgument('sortBy');
        $filter = $input->getArgument('filter');
        $format = $input->getOption('format');

        if ($sortBy) {
            if (!in_array($sortBy, ['ADDRESS', 'DEED_DATE', 'PRICE_ASC', 'PRICE_DESC'])) {
                $io->error('Please select correct sort by value!');
            }
        }

        if ($filter) {
            if ($filter[0] && !in_array($filter[0], [0.0, 0.25, 0.5, 1.0, 3.0, 5.0, 10.0, 15.0])) {
                $io->error('Please provide correct radius!');
            } else if ($filter[1] && !in_array($filter[1], [1, 2, 5, 7, 30])) {
                $io->error('Please provide correct sold in!');
            } else if ($filter[2] && !in_array($filter[2], ['ANY', 'DETACHED', 'FLAT', 'SEMI_DETACHED', 'TERRACED', 'OTHER'])) {
                $io->error('Please provide correct property type!');
            } else if (isset($filter[3]) && !in_array($filter[2], ['ANY', 'FREEHOLD', 'LEASEHOLD'])) {
                $io->error('Please provide correct tenure!');
            }

            $filter = [
                'radius' => $filter[0],
                'soldIn' => $filter[1],
                'propertyType' => $filter[2],
                'tenure' => $filter[3],
            ];
        }

        if ($format) {
            if (!in_array($format, ['json', 'table'])) {
                $io->error('Please provide correct format!');
            }
        }

        $response = $this->housePriceService->search($searchTerm, $sortBy, $filter);

        if (isset($format) && $format === 'json') {
            $output->writeln(json_encode($response));
        } else {
            $output->writeln("Number of properties sold: " . $response['number_of_sold_properties']);
            $table = new Table($output);
            $table
                ->setHeaders(['Address', 'Property Type', 'Price'])
                ->setRows($response['properties']);
            $table->render();
        }

        return Command::SUCCESS;
    }
}
