<?php

namespace App\Controller;

use App\Service\HousePriceService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HousePriceController extends AbstractController
{
    /**
     * @Route("/api/house-prices/{searchTerm}", name="house_price")
     * @param Request $request
     * @param string $searchTerm
     * @param HousePriceService $housePriceService
     * @return Response
     */
    public function housePrices(Request $request, string $searchTerm, HousePriceService $housePriceService): Response
    {
        $sortBy = $request->get('sortBy');
        $filters = [
            'radius' => $request->get('radius'),
            'soldIn' => $request->get('soldIn'),
            'propertyType' => $request->get('propertyType'),
            'tenure' => $request->get('tenure'),
        ];

        $response = $housePriceService->search($searchTerm, $sortBy, $filters);

        return $this->json($response);
    }
}
