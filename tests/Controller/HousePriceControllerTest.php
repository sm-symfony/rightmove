<?php


namespace App\Tests\Controller;


use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HousePriceControllerTest extends WebTestCase
{
    public function testHousePrices()
    {
        $client = static::createClient();
        $client->request('GET', '/api/house-prices/NW3?sortBy=PRICE_DESC&radius=0.0&soldIn=30&propertyType=ANY&tenure=ANY');
        $response = json_decode($client->getResponse()->getContent());
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertIsNumeric((int) $response->number_of_sold_properties);
        $this->assertEquals(5, count($response->properties));
    }
}